const int SAMPLESTOTAKE = 30;
const int TRESHOLD = 15;
const int DEBUG = true;

bool isDrop = false;
bool secondBeat = false;
int currentItterationIndex = 0;
int previousItterationIndex;
int samplesTotal = 0;
int samplesAverage;
int collectedSamples[SAMPLESTOTAKE];
int sampleDeviation;
unsigned long time;
unsigned long timeLenght;

void setup()
{
    Serial.begin(9600);
    pinMode(A0, INPUT);

    for (int i = 0; i < SAMPLESTOTAKE; i++)
    {
        collectedSamples[i] = 0;
    }
}

void loop()
{
    // Average sampling
    samplesTotal = 0;
    for (int i = 0; i < SAMPLESTOTAKE; i++)
    {
        samplesTotal += collectedSamples[i];
    }
    samplesAverage = (samplesTotal / SAMPLESTOTAKE);
    collectedSamples[currentItterationIndex] = (int)analogRead(A0);

    // Detect drop
    previousItterationIndex = ((currentItterationIndex - 1) % SAMPLESTOTAKE);
    sampleDeviation = abs(collectedSamples[currentItterationIndex] - collectedSamples[previousItterationIndex]);
    if (sampleDeviation > TRESHOLD)
    {
        if (!isDrop)
        {
            if (secondBeat)
            {
                timeLenght = millis() - time;
                secondBeat = false;
            }
            else
            {
                time = millis();
                secondBeat = true;
            }
        }

        isDrop = true;
    }
    else
    {
        isDrop = false;
    }


    currentItterationIndex++;
    currentItterationIndex = (currentItterationIndex % SAMPLESTOTAKE);

    // Serial debug
    if (DEBUG)
    {
        Serial.print((int)analogRead(A0));
        Serial.print(" ");

        Serial.print(samplesAverage);
        Serial.print(" ");

        Serial.print(timeLenght);
        Serial.print(" ");

        if (isDrop)
        {
            Serial.println(50);
        }
        else
        {
            Serial.println(0);
        }

    }

}